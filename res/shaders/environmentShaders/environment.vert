#version 330

in vec3 in_position;

out vec3 texCoords;

uniform mat4 uni_mat_view;
uniform mat4 uni_mat_proj;

void main()
{
	texCoords = in_position;

	vec4 pos = uni_mat_proj * uni_mat_view * vec4(in_position, 1.0);
	gl_Position = pos.xyww;
}