#version 330

layout (location = 0) out vec4 out_fragColor;

in vec3 texCoords;

uniform samplerCube uni_cubemap;

void main()
{             
    out_fragColor = texture(uni_cubemap, texCoords);
} 