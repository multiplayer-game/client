#version 330

in vec3 in_position;
in vec2 in_texCoords;
//in vec3 in_offset; // for future instanced rendering of particles

out vec2 texCoords;

uniform mat4 uni_mat_view;
uniform mat4 uni_mat_proj;
uniform mat4 uni_mat_model;

uniform vec3 uni_camera_up;
uniform vec3 uni_camera_right;

uniform vec3 uni_particle_center;
uniform float uni_particle_size;

void main()
{
	texCoords = in_texCoords;
	vec3 billboardPosition = uni_camera_right * in_position.x + uni_camera_up * in_position.y;
	gl_Position = uni_mat_proj * uni_mat_view * uni_mat_model * vec4(billboardPosition, 1.0);
}