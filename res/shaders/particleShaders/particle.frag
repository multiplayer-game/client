#version 330

in vec2 texCoords;

uniform sampler2D uni_texture;

layout (location = 0) out vec4 out_fragColor;

void main()
{
	vec4 color =  texture(uni_texture, texCoords);
	out_fragColor = color * vec4(0.0, 124.0/255.0, 155.0/255.0, 1.0);
}