#version 330

in vec2 texCoords;
in vec3 frag_position;
in vec3 normal;
in vec3 tangent;
in vec3 bitangent;

uniform sampler2D uni_diffuseTexture;
uniform sampler2D uni_specularTexture;
uniform float uni_shininess;
uniform sampler2D uni_normalMapTexture;

uniform samplerCube uni_skyboxTexture;

uniform vec3 uni_view_position;
uniform float uni_far_clipping_plane;

uniform vec3 uni_light_position[32];
uniform vec3 uni_light_color[32];
uniform int uni_light_amount;
uniform vec3 uni_directional_light_direction;
uniform vec3 uni_directional_light_color;

layout (location = 0) out vec4 out_fragColor;

void main()
{
	vec3 normTex = (texture(uni_normalMapTexture, texCoords).rgb - vec3(0.5, 0.5, 0.5)) * 2.0;
	mat3 TBN = mat3(normalize(tangent), normalize(bitangent), normalize(normal));
	vec3 norm = normalize(TBN*normTex);

	vec3 view_dir = normalize(uni_view_position - frag_position);

	vec3 diffuse_color = vec3(texture(uni_diffuseTexture, texCoords));
	vec3 specular_color = vec3(texture(uni_specularTexture, texCoords));
	vec3 skybox_color = vec3(texture(uni_skyboxTexture, -view_dir));
	vec3 skybox_reflect_color = vec3(texture(uni_skyboxTexture, reflect(-view_dir, norm)));

	vec3 diffuse = vec3(0.0, 0.0, 0.0);
	vec3 specular = vec3(0.0, 0.0, 0.0);

	// point lights
	for(int i = 0; i < uni_light_amount; i++)
	{
		vec3 light_dir = normalize(uni_light_position[i] - frag_position);
		float kd = max(dot(norm, light_dir), 0.0);
		
		vec3 reflect_dir = reflect(-light_dir, norm);
		float ks = pow(max(dot(view_dir, reflect_dir), 0.0), uni_shininess);
	
		diffuse += uni_light_color[i] * diffuse_color * kd;
		specular += uni_light_color[i] * specular_color * ks;
	}

	//directional light
	float kd = max(dot(norm, -uni_directional_light_direction), 0.0);
		
	vec3 reflect_dir = reflect(uni_directional_light_direction, norm);
	float ks = pow(max(dot(view_dir, reflect_dir), 0.0), uni_shininess);
	
	diffuse += uni_directional_light_color * diffuse_color * kd;
	specular += uni_directional_light_color * specular_color * ks;

	float fog = clamp(distance(uni_view_position, frag_position) / uni_far_clipping_plane, 0.0, 1.0);

	vec4 phong_color = vec4(diffuse + specular + skybox_reflect_color * specular_color, 1.0);

	out_fragColor = mix(phong_color, vec4(skybox_color, 1.0), pow(fog, 6));
}