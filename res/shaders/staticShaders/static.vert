#version 330

in vec3 in_position;
in vec2 in_texCoords;
in vec3 in_normal;
in vec3 in_tangent;
in vec3 in_bitangent;

out vec3 frag_position;
out vec2 texCoords;
out vec3 normal;
out vec3 tangent;
out vec3 bitangent;

uniform mat4 uni_mat_view;
uniform mat4 uni_mat_proj;
uniform mat4 uni_mat_model;

void main()
{
	vec4 frag_position4 = uni_mat_model * vec4(in_position, 1.0);
	frag_position = vec3(frag_position4) / frag_position4.w;

	texCoords = in_texCoords;

	normal = normalize(vec3(uni_mat_model * vec4(in_normal, 0.0)));
	tangent = normalize(vec3(uni_mat_model * vec4(in_tangent, 0.0)));
	bitangent = normalize(vec3(uni_mat_model * vec4(in_bitangent, 0.0)));

	gl_Position = uni_mat_proj * uni_mat_view * uni_mat_model * vec4(in_position, 1.0);
}