#version 330

in vec2 in_position;
in vec2 in_texCoords;

out vec2 texCoords;

void main()
{
    gl_Position = vec4(in_position.x, in_position.y, 0.0, 1.0); 
    texCoords = in_texCoords;
} 