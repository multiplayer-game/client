#version 330

#define M_PI 3.1415926535897932384626433832795

out vec4 FragColor;
  
in vec2 texCoords;

uniform sampler2D uni_colorTexture;
uniform sampler2D uni_depthTexture;

uniform float uni_gaussian_sigma;

float LinearizeDepth(in vec2 uv)
{
    float zNear = 0.0001f;    // TODO: Replace by the zNear of your perspective projection
    float zFar  = 10000.0f;   // TODO: Replace by the zFar  of your perspective projection
    float depth = texture2D(uni_depthTexture, uv).x;
    return (2.0 * zNear) / (zFar + zNear - depth * (zFar - zNear));
}

float gaussian_factor(int x, int y, float r)
{
	float exponent =  - ( x * x + y * y) / (2 * r * r);
	return exp(exponent) / (2 * M_PI * r * r);
}

vec4 gaussian_blur(in vec2 uv, in sampler2D textureSampler, float sigma)
{
	vec4 blured_pixel_color = vec4(0.0, 0.0, 0.0, 0.0);
	vec2 texture_size = textureSize(textureSampler, 0);
	int kernel_size = int(ceil(sigma * 6 + 1));
	float weightSum = 0.0;

	for(int i = - kernel_size; i <= kernel_size; i++)
	{
		for(int j = - kernel_size; j <= kernel_size; j++)
		{
			vec2 new_uv = uv + vec2(i/texture_size.x, j/texture_size.y);
			//blured_pixel_color += texture(textureSampler, new_uv) * min(gaussian_factor(i, j, sigma), 1.0);
			float g = gaussian_factor(i, j, sigma);
			blured_pixel_color += texture(textureSampler, new_uv) * g;
			weightSum += g;
		}
	}
	return blured_pixel_color/weightSum;
}

void main()
{
	// depth handling
	vec2 screenSize = textureSize(uni_colorTexture, 0);
	float depth = texture2D(uni_depthTexture, texCoords).x;
	float depthCap = texture2D(uni_depthTexture, vec2(0.5, 0.5)).x;

	// Depth texture linearization
	float linearizedDepth = LinearizeDepth(texCoords);
	float linenearizedDepthCap = LinearizeDepth(vec2(0.5, 0.5));

	// texture handling
    vec4 depthTexture = vec4(linearizedDepth, linearizedDepth, linearizedDepth, 1.0);
	vec4 colorTexture = texture2D(uni_colorTexture, texCoords);

	vec4 finalColor;

	if(depth > depthCap * 1.000002)
	{
		float factor = (depth - depthCap * 1.000002) / (1.0 - depthCap * 1.000002);
		finalColor = gaussian_blur(texCoords, uni_colorTexture, uni_gaussian_sigma * factor);
	}
	else if(depth < depthCap * 0.999998)
	{
		float factor = (depthCap * 0.999998 - depth) / (1.0 - depth);
		finalColor = gaussian_blur(texCoords, uni_colorTexture, uni_gaussian_sigma * factor);
	}
	else
		finalColor = colorTexture;
	FragColor = finalColor;
}