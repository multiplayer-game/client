#pragma once

#include "../Models/MaterialModel.h"

namespace Engine
{
	class BaseEntity
	{
	private:
		MaterialModel * m_model;

	public:
		BaseEntity() {}

		~BaseEntity()
		{
			delete m_model;
		}
	};
}