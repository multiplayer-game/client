#pragma once

#include <vector>
#include <GL/glew.h>
#include <GL/GL.h>
#include "../Shaders/ShaderProgram.h"
#include "EBO.h"
#include "VBO.h"
#include <assert.h>

namespace Engine
{
	class VAO
	{
	private:
		GLuint m_id;
		size_t m_eboSize;

		template <typename Type>
		static std::vector<std::pair<GLint, Type>> convert(const ShaderProgram & program, const std::vector<std::pair<std::string, Type>> & vbos)
		{
			std::vector<std::pair<GLint, Type>> result;
			result.reserve(vbos.size());
			for (auto it = vbos.begin(), end = vbos.end(); it != end; ++it)
			{
				GLint location = program.getAttributeLocation(it->first);
				assert(location != -1);
				result.push_back({ location, it->second });
			}
			return std::move(result);
		}

	public:
		VAO(const std::vector<std::pair<GLint, const VBO *>> & vbo, const EBO * ebo = nullptr)
		{
			// Creation and binding of the vertex array object
			glGenVertexArrays(1, &m_id);
			glBindVertexArray(m_id);
			// Association of vertex buffer objects with the vertex array object and the shader program
			for (auto it = vbo.begin(), end = vbo.end(); it != end; ++it)
			{
				const std::pair<GLuint, const VBO *> & current = (*it);
				current.second->attribPointer(current.first);
			}
			// Association of the element buffer object with the vertex array object
			if (ebo != nullptr) { ebo->bind(); }
			glBindVertexArray(0);
			m_eboSize = ebo->getSize();
		}

		VAO(const ShaderProgram & program, const std::vector<std::pair<std::string, const VBO *>> & vbos, const EBO * ebo = nullptr) : VAO(convert(program, vbos), ebo) {}

		~VAO() { glDeleteVertexArrays(1, &m_id); }

		bool isValid() const { return m_id != 0; }

		size_t eboSize() const { return m_eboSize; }

		void bind() const { assert(isValid()); glBindVertexArray(m_id); }

		void unbind() const { glBindVertexArray(0); }
	};
}