#pragma once

#include <GL\glew.h>
#include <GL\GL.h>
#include <vector>
#include <assert.h>

namespace Engine
{
	class EBO
	{
	private:
		GLuint m_id;
		size_t m_dataSize;

	public:
		// Creates an EBO containing the indices stored in "buffer"
		EBO(const std::vector<GLuint> & buffer, GLenum usage = GL_STATIC_DRAW)
		{
			glGenBuffers(1, &m_id);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_id);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint)*buffer.size(), buffer.data(), usage);
			m_dataSize = buffer.size();
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		}

		// Creates an empty EBO
		EBO() : m_id(0), m_dataSize(0) {}

		// Deletes the buffer
		~EBO()
		{
			glDeleteBuffers(1, &m_id);
		}

		// Returns true if the VBO is valid
		bool isValid() const { return m_id != 0; }

		// Returns the ID of the EBO (returns 0 if the EBO is not valid)
		GLuint getId()  const { return m_id; }

		// Returns the size of the data contained in the EBO
		size_t getSize() const { return m_dataSize; }

		// Binds the VBO
		void bind() const { assert(isValid()); glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_id); }

		// Unbinds the VBO
		void unbind() const { glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); }
	};
}
