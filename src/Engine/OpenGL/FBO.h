#pragma once

#include <GL/glew.h>
#include <GL/GL.h>
#include "../Textures/Texture.h"
#include "../Shaders/ShaderProgram.h"
#include "../Shaders/ShaderParser.h"
#include "../Shaders/Shader.h"
#include "VAO.h"
#include "EBO.h"
#include "VBO.h"
#include <iostream>

namespace Engine
{
	class FBO
	{
	private:
		int m_height;
		int m_width;

		GLuint m_id;

		GLuint m_colorTexture;
		GLuint m_depthTexture;

		ShaderProgram * m_shaderProgram;
		Shader * m_vertexShader;
		Shader * m_fragmentShader;

		VAO * m_vao;

		// generates VAO for rendering frambuffer on whole screen
		void genModel()
		{
			/// Vertices
			std::vector<glm::vec2> vertices = {
				glm::vec2(-1.0f, 1.0f),
				glm::vec2(-1.0f, -1.0f),
				glm::vec2(1.0f, 1.0f),
				glm::vec2(1.0f, -1.0f)
			};
			/// Texture Coordinates
			std::vector<glm::vec2> texCoords = {
				glm::vec2(0.0, 1.0),
				glm::vec2(0.0, 0.0),
				glm::vec2(1.0, 1.0),
				glm::vec2(1.0, 0.0)
			};
			/// Indexes for the EBO
			std::vector<GLuint> indices = { 0, 1, 2, 1, 3, 2 };

			// VAO creation
			std::vector<std::pair<std::string, const VBO *>> vbos;
			VBO * verticeVBO = new VBO(vertices);
			VBO * texCoordsVBO = new VBO(texCoords);
			EBO * indicesEBO = new EBO(indices);
			vbos.push_back(std::pair("in_position", verticeVBO));
			vbos.push_back(std::pair("in_texCoords", texCoordsVBO));
			m_vao = new VAO(*m_shaderProgram, vbos, indicesEBO);
		}

		// loads shader program for rendering framebuffer on whole screen
		void loadShader()
		{
			ShaderParser * parser = new ShaderParser();
			std::string path = (parser->getShaderPath() / "framebufferRenderingShaders" / "framebufferRendering.vert").string();
			std::string file = parser->parseFile(path);
			m_vertexShader = new Shader(path, file, GL_VERTEX_SHADER);
			path = (parser->getShaderPath() / "framebufferRenderingShaders" / "framebufferRendering.frag").string();
			file = parser->parseFile(path);
			m_fragmentShader = new Shader(path, file, GL_FRAGMENT_SHADER);
			m_shaderProgram = new ShaderProgram(m_vertexShader, m_fragmentShader);
			delete parser;
		}

	public:
		FBO(int height, int width, bool colorBuffer = true, bool depthBuffer = true) : m_height(height), m_width(width)
		{
			// framebuffer creation
			glGenFramebuffers(1, &m_id);
			glBindFramebuffer(GL_FRAMEBUFFER, m_id);

			// color buffer creation
			if (colorBuffer)
			{
				if(LOG_DEBUG_MESSAGE) std::cout << "Generating color texture for FBO !" << std::endl;
				glGenTextures(1, &m_colorTexture);
				glBindTexture(GL_TEXTURE_2D, m_colorTexture);

				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

				glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_colorTexture, 0);
				glBindTexture(GL_TEXTURE_2D, 0);
			}

			// depth buffer creation
			if (depthBuffer)
			{
				if(LOG_DEBUG_MESSAGE) std::cout << "Generating depth texture for FBO !" << std::endl;
				glGenTextures(1, &m_depthTexture);
				glBindTexture(GL_TEXTURE_2D, m_depthTexture);

				glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

				glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_depthTexture, 0);
				glBindTexture(GL_TEXTURE_2D, 0);
			}

			// buffer attribution to framebuffer
			GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
			glDrawBuffers(1, DrawBuffers);

			// checks framebuffer completeness
			if (!glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE && LOG_DEBUG_MESSAGE)
				std::cout << "OpenGL error while creating framebuffer !" << std::endl;

			// checks for OpenGL errors
			if (glGetError() != GL_NO_ERROR && LOG_DEBUG_MESSAGE)
			{
				std::cout << "OpenGL error after FBO creation !" << std::endl;
			}

			// unbinds framebuffer
			glBindFramebuffer(GL_FRAMEBUFFER, 0);

			// loads shader program to render framebuffer on screen & generates mendatory model
			loadShader();
			genModel();
		}
		~FBO()
		{
			// memory cleanup
			glDeleteTextures(1, &m_colorTexture);
			glDeleteRenderbuffersEXT(1, &m_depthTexture);
			glDeleteFramebuffers(1, &m_id);
			glBindRenderbuffer(GL_FRAMEBUFFER, 0);

			delete m_shaderProgram;
			delete m_vertexShader;
			delete m_fragmentShader;

			delete m_vao;
		}

		// resizes the framebuffer
		void resize(int height, int width)
		{
			glBindTexture(GL_TEXTURE_2D, m_colorTexture);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);

			glBindTexture(GL_TEXTURE_2D, m_depthTexture);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
			glBindTexture(GL_TEXTURE_2D, 0);
		}

		// renders the color buffer on screen
		void renderToScreen()
		{
			m_shaderProgram->useProgram();
			m_vao->bind();
			//glDisable(GL_DEPTH_TEST);
			bindColorBuffer(0, "uni_colorTexture");
			bindDepthBuffer(1, "uni_depthTexture");
			updateUniforms();
			glDrawElements(GL_TRIANGLES, m_vao->eboSize(), GL_UNSIGNED_INT, 0);
			glBindTexture(GL_TEXTURE_2D, 0);
			//glEnable(GL_DEPTH_TEST);
			m_vao->unbind();
			m_shaderProgram->unuseProgram();
		}

		void updateUniforms()
		{
			float sigma = 2.0;
			GLint uniSigma = glGetUniformLocation(m_shaderProgram->getId(), "uni_gaussian_sigma");
			glUniform1fv(uniSigma, 1, &sigma);
		}

		// binds the color buffer texture for rendering
		void bindColorBuffer(GLuint textureUnit, char * uniformName)
		{
			glActiveTexture(GL_TEXTURE0 + textureUnit);
			glBindTexture(GL_TEXTURE_2D, m_colorTexture);
			GLint uniTexture = glGetUniformLocation(m_shaderProgram->getId(), uniformName);
			glUniform1i(uniTexture, textureUnit);
		}

		// binds the depth buffer texture for rendering
		void bindDepthBuffer(GLuint textureUnit, char * uniformName)
		{
			glActiveTexture(GL_TEXTURE0 + textureUnit);
			glBindTexture(GL_TEXTURE_2D, m_depthTexture);
			GLint uniTexture = glGetUniformLocation(m_shaderProgram->getId(), uniformName);
			glUniform1i(uniTexture, textureUnit);
		}

		// clears the framebufer
		void clearBuffer()
		{
			bind();
			glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			unbind();
		}

		// set this framebuffer as the rendering target
		void bind() { glBindFramebuffer(GL_FRAMEBUFFER, m_id); }

		// set rendering target back to default framebuffer
		void unbind() { glBindFramebuffer(GL_FRAMEBUFFER, 0); }
	};
}