#pragma once

#include <GL\glew.h>
#include <GL\GL.h>
#include <vector>
#include <assert.h>
#include "../GLM/TypeTraits.h"

namespace Engine
{
	class VBO
	{
	private:
		GLuint m_id;
		size_t m_dataSize;
		size_t m_vectorSize;
		GLenum m_scalarType;

	public:
		// Creates a VBO containing the data in "buffer"
		template <class T>
		VBO(const std::vector<T> & buffer, GLenum usage = GL_STATIC_DRAW)
		{
			static_assert(TypeTraits<T>::isCompatible() && (TypeTraits<T>::isScalar() || TypeTraits<T>::isVector()), "Invalid type provided in VBO constructor");
			glGenBuffers(1, &m_id);

			glBindBuffer(GL_ARRAY_BUFFER, m_id);
			glBufferData(GL_ARRAY_BUFFER, sizeof(T)*buffer.size(), buffer.data(), usage);
			m_scalarType = GLenum(TypeTraits<T>::glType());
			m_vectorSize = TypeTraits<T>::size();
			m_dataSize = buffer.size();
			glBindBuffer(GL_ARRAY_BUFFER, 0);
		}

		// Creates an empty VBO
		VBO() : m_id(-1), m_dataSize(0) {}

		// Deletes the buffer
		~VBO()
		{
			glDeleteBuffers(1, &m_id);
		}

		// Returns true if the VBO is valid
		bool isValid() const { return m_id != 0; }

		// Returns the ID of the VBO (returns 0 if the VBO is not valid)
		GLuint getId()  const { return m_id; }

		// Returns the type of scalar stored in the VBO
		GLenum glScalarType() const { assert(isValid()); return m_scalarType; }

		// Returns the dimension of scalars stored in the VBO
		unsigned int vectorDimension() const { assert(isValid()); return m_vectorSize; }

		// Returns the size of the data contained in the VBO
		size_t getSize() const { return m_dataSize; }

		// Binds the VBO
		void bind() const { assert(isValid()); glBindBuffer(GL_ARRAY_BUFFER, m_id); }

		// Unbinds the VBO
		void unbind() const { glBindBuffer(GL_ARRAY_BUFFER, 0); }

		void attribPointer(GLint shaderAttributeIndex, GLuint nbInstances = 0) const
		{
			bind();
			// We associate the buffer with the input of the shader program
			glVertexAttribPointer(shaderAttributeIndex, vectorDimension(), glScalarType(), GL_FALSE, 0, (void*)0);
			glEnableVertexAttribArray(shaderAttributeIndex);
			if (nbInstances > 0) { glVertexAttribDivisor(shaderAttributeIndex, nbInstances); }
		}
	};
}