#pragma once

#include <GL/glew.h>

namespace Engine
{
	enum class BufferUpdate : GLenum {
		staticDraw = GL_STATIC_DRAW,
		dynamicDraw = GL_DYNAMIC_DRAW,
		streamDraw = GL_STREAM_DRAW
	};
}