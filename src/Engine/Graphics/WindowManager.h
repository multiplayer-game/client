#pragma once

#include <string>
#include <iostream>
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include <GL\GL.h>
#include "../Config/config.h"

void framebuffer_size_callback(GLFWwindow* window, int width, int height);

void GLAPIENTRY MessageCallback(GLenum source,
	GLenum type,
	GLuint id,
	GLenum severity,
	GLsizei length,
	const GLchar* message,
	const void* userParam);

namespace Engine
{
	class WindowManager
	{
	private:
		GLFWwindow * m_window;
		boolean m_windowCloseRequested;

	public:
		WindowManager() : m_windowCloseRequested(false) {};

		~WindowManager() {};

		void createWindow(int height, int width, std::string name)
		{
			if(LOG_DEBUG_MESSAGE) std::cout << "Creating window : " << name << " !" << std::endl;

			if (!glfwInit() && LOG_DEBUG_MESSAGE) { std::cout << "Error initializing glfw !" << std::endl; }

			// GLFW parameters for window creation
			glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
			glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4); // for OpenGL 4.4
			glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
			//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // should not be necessary but whatever MacOS
			glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE); // old-school OpenGL compatibility

			m_window = glfwCreateWindow(width, height, name.c_str(), NULL, NULL);
			if (m_window == NULL) {
				if(LOG_DEBUG_MESSAGE) fprintf(stderr, "Failed to open GLFW window\n");
				glfwTerminate();
			}
			glfwMakeContextCurrent(m_window); // Initialize GLEW
			glfwSwapInterval(0); // Disables forced vsync
			glewExperimental = true; // Mandatory with base profile

			if (glewInit() != GLEW_OK && LOG_DEBUG_MESSAGE) {
				fprintf(stderr, "Failed to initialize GLEW\n");
			}
			if(LOG_DEBUG_MESSAGE) fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));

			// During init, enable debug output
			glEnable(GL_DEBUG_OUTPUT);
			glDebugMessageCallback(MessageCallback, 0);

			const char * description[1024];

			while (glfwGetError(description) != 0 && LOG_DEBUG_MESSAGE)	std::cout << "GLFW error ! " << *description << std::endl;

			glfwSetFramebufferSizeCallback(m_window, framebuffer_size_callback);

			if (glGetError() != GL_NO_ERROR && LOG_DEBUG_MESSAGE) std::cout << "OpenGL error after window creation ! " << std::endl;
		}

		void updateWindow()
		{
			glfwSwapBuffers(m_window);
			glfwPollEvents();
			if (glfwWindowShouldClose(m_window)) { m_windowCloseRequested = true; }
		}

		void closeWindow()
		{
			if (m_windowCloseRequested)
				glfwDestroyWindow(m_window);
		}

		bool windowCloseRequested()
		{
			return m_windowCloseRequested;
		}

		GLFWwindow * getWindow() { return m_window; }
	};
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void GLAPIENTRY MessageCallback(GLenum source,
	GLenum type,
	GLuint id,
	GLenum severity,
	GLsizei length,
	const GLchar* message,
	const void* userParam)
{
	if(LOG_DEBUG_MESSAGE)
		fprintf(stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
		(type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
		type, severity, message);
}