#pragma once

#include <GL/glew.h>
#include <GL/GL.h>
#include <string>
#include <glm/glm.hpp>
#include <glm/common.hpp>
#include <GLFW/glfw3.h>
#include "../Shaders/ShaderProgram.h"
#include "../Shaders/Shader.h"
#include "../Shaders/ShaderParser.h"
#include "../OpenGL/VBO.h"
#include "../OpenGL/EBO.h"
#include "../OpenGL/VAO.h"
#include "Camera.h"
#include "../Models/MaterialModel.h"
#include "../Lighting/LightManager.h"
#include "../Lighting/PointLight.h"
#include "../Config/config.h"
#include "../Environment/EnvironmentManager.h"

namespace Engine
{
	class StaticRenderer
	{
	private:
		ShaderProgram * m_shaderProgram;
		Shader * m_vertexShader;
		Shader * m_fragmentShader;

		LightManager & m_lightManager;
		EnvironmentManager & m_environmentManager;

		std::vector<MaterialModel *> m_models;

	public:
		StaticRenderer(LightManager & lightManager, EnvironmentManager & environmentManager) : m_lightManager(lightManager), m_environmentManager(environmentManager) {}

		~StaticRenderer()
		{
			delete m_vertexShader;
			delete m_fragmentShader;
			delete m_shaderProgram;
		}

		void init()
		{
			ShaderParser * parser = new ShaderParser();
			std::string path = (parser->getShaderPath() / "staticShaders" / "static.vert").string();
			std::string file = parser->parseFile(path);
			m_vertexShader = new Shader(path, file, GL_VERTEX_SHADER);
			path = (parser->getShaderPath() / "staticShaders" / "static.frag").string();
			file = parser->parseFile(path);
			m_fragmentShader = new Shader(path, file, GL_FRAGMENT_SHADER);
			m_shaderProgram = new ShaderProgram(m_vertexShader, m_fragmentShader);
			delete parser;
		}

		void addModel(MaterialModel * model) { m_models.push_back(model); }

		// renders all the static objects in the scene
		void render(GLFWwindow * window, Camera * camera, long dt)
		{
			m_shaderProgram->useProgram();

			for (MaterialModel * model : m_models)
			{
				model->prepareForRender(*m_shaderProgram);

				model->bindMaterial(0, 1, 2, *m_shaderProgram);

				m_environmentManager.getSbybox()->updateUniforms(3, "uni_skyboxTexture", *m_shaderProgram);

				updateUniforms(window, camera, dt, model);

				glDrawElements(GL_TRIANGLES, model->getEboSize(), GL_UNSIGNED_INT, 0);

				model->unbind();
			}

			m_shaderProgram->unuseProgram();
		}

		void updateUniforms(GLFWwindow * window, Camera * camera, long dt, MaterialModel * model)
		{
			// projection matrix construction
			int width, height;
			glfwGetWindowSize(window, &width, &height);
			float farClippingPlane = FAR_CLIPPING_PLANE;
			glm::mat4 proj_mat = glm::perspective(glm::pi<float>() / 2.0f, (float)width / (float)height, FRONT_CLIPPING_PLANE, farClippingPlane);

			// view matrix uniform update
			GLint uniView = glGetUniformLocation(m_shaderProgram->getId(), "uni_mat_view");
			glUniformMatrix4fv(uniView, 1, GL_FALSE, &(camera->getInverseTransform()[0][0]));

			// projection matrix uniform update
			GLint uniProjection = glGetUniformLocation(m_shaderProgram->getId(), "uni_mat_proj");
			glUniformMatrix4fv(uniProjection, 1, GL_FALSE, &proj_mat[0][0]);

			// model matrix uniform update
			GLint uniModel = glGetUniformLocation(m_shaderProgram->getId(), "uni_mat_model");
			glUniformMatrix4fv(uniModel, 1, GL_FALSE, &model->getModelMatrix()[0][0]);

			// camera position uniform update
			GLint uniViewPosition = glGetUniformLocation(m_shaderProgram->getId(), "uni_view_position");
			glUniform3fv(uniViewPosition, 1, &camera->getPosition()[0]);

			// camera position uniform update
			GLint uniFarClippingPlane = glGetUniformLocation(m_shaderProgram->getId(), "uni_far_clipping_plane");
			glUniform1fv(uniFarClippingPlane, 1, &farClippingPlane);

			// Below this needs re-work
			glm::vec3 lightsColors[32] = { glm::vec3(0.0, 0.0, 0.0) };
			glm::vec3 lightsPositions[32] = { glm::vec3(0.0, 0.0, 0.0) };
			GLint lightAmount = m_lightManager.getPointLightAmount();

			for (int i = 0; i < lightAmount && i < 32; i++)
			{
				lightsColors[i] = m_lightManager.getPointLights()[i].getColor();
				lightsPositions[i] = m_lightManager.getPointLights()[i].getPosition();
			}

			GLint uniLightPosition = glGetUniformLocation(m_shaderProgram->getId(), "uni_light_position");
			glUniform3fv(uniLightPosition, lightAmount, &lightsPositions[0][0]);

			GLint uniLightColor = glGetUniformLocation(m_shaderProgram->getId(), "uni_light_color");
			glUniform3fv(uniLightColor, lightAmount, &lightsColors[0][0]);

			GLint uniLightAmount = glGetUniformLocation(m_shaderProgram->getId(), "uni_light_amount");
			glUniform1iv(uniLightAmount, 1, &lightAmount);

			GLint uniDirectionalLightDirection = glGetUniformLocation(m_shaderProgram->getId(), "uni_directional_light_direction");
			glUniform3fv(uniDirectionalLightDirection, 1, &m_lightManager.getDirectionnalLights()[0].getDirection()[0]);

			GLint uniDirectionalLightColor = glGetUniformLocation(m_shaderProgram->getId(), "uni_directional_light_color");
			glUniform3fv(uniDirectionalLightColor, 1, &m_lightManager.getDirectionnalLights()[0].getColor()[0]);
		}
	};
}