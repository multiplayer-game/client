#pragma once

#include <vector>
#include "..\Particles\ParticleSystem.h"
#include "..\Shaders\ShaderProgram.h"
#include "..\Shaders\Shader.h"
#include "..\Graphics\Camera.h"
#include "..\Loaders\TextureLoader.h"

#define RAND_F static_cast <float> (rand()) / static_cast <float> (RAND_MAX)

namespace Engine
{
	class ParticleRenderer
	{
	private:
		ShaderProgram * m_shaderProgram;
		Shader * m_vertexShader;
		Shader * m_fragmentShader;

		std::vector<ParticleSystem *> m_particleSystems;

	public:
		ParticleRenderer() {}

		~ParticleRenderer()
		{
			for (ParticleSystem * ps : m_particleSystems)
			{
				delete ps;
			}
		}

		void init()
		{
			// SHADER INIT STUFF
			ShaderParser * parser = new ShaderParser();
			std::string path = (parser->getShaderPath() / "particleShaders" / "particle.vert").string();
			std::string file = parser->parseFile(path);
			m_vertexShader = new Shader(path, file, GL_VERTEX_SHADER);
			path = (parser->getShaderPath() / "particleShaders" / "particle.frag").string();
			file = parser->parseFile(path);
			m_fragmentShader = new Shader(path, file, GL_FRAGMENT_SHADER);
			m_shaderProgram = new ShaderProgram(m_vertexShader, m_fragmentShader);
			delete parser;

			// TEST STUFF, NEEDS TO GO

			TextureLoader textureLoader = TextureLoader();

			ParticleSystem * ps = new ParticleSystem(m_shaderProgram);
			for (int i = 0; i < 1000; i++)
			{
				Particle * p = new Particle(glm::vec3(0.0, 1.0, 1.0), glm::vec3((RAND_F  * 2.0 - 1.0) * 4.0, RAND_F  * 2.0 - 1.0 + 8.0, RAND_F * 2.0 - 1.0), 0.05, RAND_F * 10.0, RAND_F, 0.2);
				ps->addParticle(*p);
			}
			ps->setTexture(textureLoader.loadTexture(textureLoader.getTexturePath() / "alpha_particle.png"));
			m_particleSystems.push_back(ps);
		}

		// Adds a particle system to the renderer
		void addParticleSystem(ParticleSystem * particleSystem)
		{
			m_particleSystems.push_back(particleSystem);
		}

		// TODO : not implemented yet
		void removeParticleSystem() {}

		// Renders all the particle systems in the scene
		void render(GLFWwindow * window, Camera * camera, long dt)
		{
			for (ParticleSystem * p : m_particleSystems)
			{
				p->render(camera, window, dt);
			}
		}
	};
}