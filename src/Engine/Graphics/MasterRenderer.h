#pragma once

#include <GL\glew.h>
#include <GL\GL.h>
#include <GLFW\glfw3.h>
#include <chrono>
#include "..\Shaders\ShaderProgram.h"
#include "..\Shaders\Shader.h"
#include "StaticRenderer.h"
#include "ParticleRenderer.h"
#include "Camera.h"
#include "../Lighting/LightManager.h"
#include "../Environment/EnvironmentManager.h"
#include "../Config/config.h"
#include "../OpenGL/FBO.h"

namespace Engine
{
	class MasterRenderer
	{
	private:
		// Render Utilities
		Camera * m_camera;
		int m_height;
		int m_width;

		// Renderers
		StaticRenderer * m_staticRenderer;
		ParticleRenderer * m_particleRenderer;

		// Managers
		LightManager * m_lightManager;
		EnvironmentManager * m_environmentManager;

		// Frame Buffers for post processing of rendering
		FBO * m_frameBuffer;

		void checkWindowSize(GLFWwindow * window)
		{
			int windowHeight, windowWidth;
			glfwGetWindowSize(window, &windowWidth, &windowHeight);
			if (windowWidth != m_width || windowHeight != m_height)
				m_frameBuffer->resize(windowHeight, windowWidth);
		}

	public:
		MasterRenderer() : m_height(480), m_width(600) {}

		~MasterRenderer()
		{
			delete m_staticRenderer;
			delete m_particleRenderer;

			delete m_camera;

			delete m_lightManager;
			delete m_environmentManager;
		}

		// master renderer initialization
		void init()
		{
			int major, minor;
			glGetIntegerv(GL_MAJOR_VERSION, &major);
			glGetIntegerv(GL_MINOR_VERSION, &minor);

			if(LOG_DEBUG_MESSAGE) std::cout << "OpenGL Version : " << major << "." << minor << std::endl;

			glEnable(GL_CULL_FACE);
			glCullFace(GL_BACK);
			glEnable(GL_DEPTH_TEST);
			glEnable(GL_TEXTURE_2D);
			glEnable(GL_TEXTURE_CUBE_MAP);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

			m_camera = new Camera();

			m_lightManager = new LightManager();
			m_environmentManager = new EnvironmentManager();

			m_staticRenderer = new StaticRenderer(*m_lightManager, *m_environmentManager);
			m_particleRenderer = new ParticleRenderer();

			m_frameBuffer = new FBO(m_height, m_width);

			if (glGetError() != GL_NO_ERROR)
			{
				std::cout << "OpenGL error before static renderer init ! " << std::endl;
			}
			m_staticRenderer->init();
			if (glGetError() != GL_NO_ERROR)
			{
				std::cout << "OpenGL error before particle renderer init ! " << std::endl;
			}
			m_particleRenderer->init();
			if (glGetError() != GL_NO_ERROR)
			{
				std::cout << "OpenGL error after every renderer init ! " << std::endl;
			}

			m_environmentManager->init();

			//TEST SCENE (might leak memory from not cleaning up afterwards)
			for (int i = 0; i < LIGHT_COUNT; i++)
			{
				PointLight * l = new PointLight(glm::vec3(RAND_F * 10.0 - 5.0, RAND_F * 5.0, RAND_F * 10.0 - 5.0), glm::vec3(RAND_F + 0.1, RAND_F + 0.1, RAND_F + 0.1), 1.0);
				m_lightManager->addPointLight(*l);
			}

			DirectionnalLight * l = new DirectionnalLight(glm::vec3(1.0, 1.0, 1.0));
			m_lightManager->addDirectionnalLight(*l);

			std::vector<glm::vec3> vertices = {
				glm::vec3(-0.5f, 0.5f, 0.0f),
				glm::vec3(-0.5f, -0.5f, 0.0f),
				glm::vec3(0.5f, 0.5f, 0.0f),
				glm::vec3(0.5f, -0.5f, 0.0f)
			};
			/// Texture Coordinates
			std::vector<glm::vec2> texCoords = {
				glm::vec2(0.0, 1.0),
				glm::vec2(0.0, 0.0),
				glm::vec2(1.0, 1.0),
				glm::vec2(1.0, 0.0)
			};
			/// Indexes for the EBO
			std::vector<GLuint> indices = { 0, 1, 2, 1, 3, 2 };

			/// Normals
			std::vector<glm::vec3> normals = { glm::vec3(0.0, 0.0, 1.0), glm::vec3(0.0, 0.0, 1.0), glm::vec3(0.0, 0.0, 1.0), glm::vec3(0.0, 0.0, 1.0) };

			std::vector<glm::vec3> tangents = { glm::vec3(1.0, 0.0, 0.0), glm::vec3(1.0, 0.0, 0.0), glm::vec3(1.0, 0.0, 0.0), glm::vec3(1.0, 0.0, 0.0) };

			std::vector<glm::vec3> bitangents = { glm::vec3(0.0, 1.0, 0.0), glm::vec3(0.0, 1.0, 0.0), glm::vec3(0.0, 1.0, 0.0), glm::vec3(0.0, 1.0, 0.0) };

			RawModel * rawModel = new RawModel();
			rawModel->setVertices(vertices);
			rawModel->setTextureCoordinates(texCoords);
			rawModel->setIndices(indices);
			rawModel->setNormals(normals);
			rawModel->setTangents(tangents);
			rawModel->setBitangents(bitangents);
			Material * sand = new Material(TextureLoader::getTexturePath() / "sand" / "sand_diffuse.png", DEFAULT_BLACK_TEXTURE_PATH, TextureLoader::getTexturePath() / "sand" / "sand_normals.png");
			Material * pavement = new Material(TextureLoader::getTexturePath() / "pavement" / "pavement_diffuse.png", DEFAULT_SPECULAR_TEXTURE_PATH, TextureLoader::getTexturePath() / "pavement" / "pavement_normals.png");

			int tileMapSize = 4 / 2;

			for (int i = -tileMapSize; i < 1 + tileMapSize; i++)
			{
				for (int j = -tileMapSize; j < 1 + tileMapSize; j++)
				{
					MaterialModel * model;
					if (i != 0 || j != 0)
						model = new MaterialModel(rawModel, (i>0)? sand : pavement);
					else
						model = new MaterialModel(rawModel, new Material(DEFAULT_BLACK_TEXTURE_PATH, DEFAULT_MIRROR_TEXTURE_PATH));
					model->translate(glm::vec3(1.0*j, -0.2, 1.0*i));
					model->rotate(glm::vec3(1.0, 0.0, 0.0), -M_PI / 2);
					m_staticRenderer->addModel(model);
				}
			}
		}

		// memory cleanup
		void cleanup()
		{
			TextureLoader tl = TextureLoader();
			tl.cleanup();
		}

		// prepares for rendering the scene
		void prepareRender()
		{
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			m_frameBuffer->clearBuffer();
		}

		// Renders the entirety of the scene
		void renderAll(GLFWwindow * window, long dt, bool displayRenderInfo)
		{
			auto before = std::chrono::high_resolution_clock::now();

			// updates the camera
			m_camera->update(window, dt);

			// checks if the window has been resized to change the size of the main frame buffer
			checkWindowSize(window);

			// binds the main frame buffer to allow for post processing
			m_frameBuffer->bind();

			auto renderPreparation = std::chrono::high_resolution_clock::now();

			// renders statics objects in the scene
			m_staticRenderer->render(window, m_camera, dt);
			glFinish();

			auto staticRendering = std::chrono::high_resolution_clock::now();

			// renders the environement in the scene
			m_environmentManager->render(window, m_camera);
			glFinish();

			auto environmentRendering = std::chrono::high_resolution_clock::now();

			// renders the particles in the scene
			//m_particleRenderer->render(window, m_camera, dt);

			auto particleRendering = std::chrono::high_resolution_clock::now();
			glFinish();

			// unbinds the main frame buffer
			m_frameBuffer->unbind();

			// renders the main frame buffer on the screen with post processing
			m_frameBuffer->renderToScreen();
			glFinish();

			auto after = std::chrono::high_resolution_clock::now();
			if (displayRenderInfo)
			{
				std::cout << "Total render time : " << (float)std::chrono::duration_cast<std::chrono::nanoseconds>(after - before).count() / (float)1000000 << "ms" << std::endl;
				std::cout << "Render preparation time : " << (float)std::chrono::duration_cast<std::chrono::nanoseconds>(renderPreparation - before).count() / (float)1000000 << "ms" << std::endl;
				std::cout << "Static object render time : " << (float)std::chrono::duration_cast<std::chrono::nanoseconds>(staticRendering - renderPreparation).count() / (float)1000000 << "ms" << std::endl;
				std::cout << "Environment render time : " << (float)std::chrono::duration_cast<std::chrono::nanoseconds>(environmentRendering - staticRendering).count() / (float)1000000 << "ms" << std::endl;
				std::cout << "Particles render time : " << (float)std::chrono::duration_cast<std::chrono::nanoseconds>(particleRendering - environmentRendering).count() / (float)1000000 << "ms" << std::endl;
				std::cout << "Post-processing render time : " << (float)std::chrono::duration_cast<std::chrono::nanoseconds>(after - particleRendering).count() / (float)1000000 << "ms" << std::endl;
			}
		}
	};
}