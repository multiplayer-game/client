#pragma once

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <GLFW/glfw3.h>

namespace Engine
{
	class Camera
	{
	private:
		glm::vec3 m_position;

		glm::mat4 m_orientation;

		float m_cameraSpeed;

		double m_previousCursorX, m_previousCursorY;
		float m_cursorSensitivity;

	public:
		Camera() : m_orientation(glm::mat4(1.0f)), m_position(0.0f, 0.0f, 1.0f), m_cameraSpeed(0.5f),
			m_previousCursorX(0.0), m_previousCursorY(0), m_cursorSensitivity(0.5f) {}

		void setPosition(glm::vec3 const & pos) { m_position = pos; }

		const glm::vec3 & getPosition() const { return m_position; }

		void translateLocal(glm::vec3 const & translation) { m_position += glm::vec3(m_orientation * glm::vec4(translation, 1.0)); }

		void translateFront(float value) { translateLocal(glm::vec3(0.0f, 0.0f, -value)); }

		void translateUp(float value) { translateLocal(glm::vec3(0.0f, value, 0.0f)); }

		void translateRight(float value) { translateLocal(glm::vec3(value, 0.0f, 0.0f)); }

		void rotateLocal(const glm::vec3 & axis, float angle) { m_orientation = m_orientation * glm::rotate(angle, axis); }

		void rotateUp(float angle) { rotateLocal((glm::vec4(0.0f, 1.0f, 0.0f, 1.0f) * m_orientation), angle); }

		void rotateRight(float angle) { rotateLocal(glm::vec3(1.0f, 0.0f, 0.0f), angle); }

		glm::mat4x4 getTransform() const { return glm::translate(m_position) * m_orientation; }

		glm::mat4x4 getInverseTransform() const { return glm::inverse(getTransform()); }

		glm::mat4x4 getOrientation() const { return m_orientation; }

		void update(GLFWwindow * window, long dt)
		{
			// if mouse not grabbed but user clicks on windows, grab mouse
			if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS && glfwGetInputMode(window, GLFW_CURSOR) == GLFW_CURSOR_NORMAL)
			{
				glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
				glfwGetCursorPos(window, &m_previousCursorX, &m_previousCursorY);
			}

			double delta = (double)dt / (double)500000000;

			// if mouse is grabbed, update camera rotations based on mouse movement
			if (glfwGetInputMode(window, GLFW_CURSOR) == GLFW_CURSOR_DISABLED)
			{
				double mouseX, mouseY;
				glfwGetCursorPos(window, &mouseX, &mouseY);

				float xoffset = mouseX - m_previousCursorX;
				float yoffset = m_previousCursorY - mouseY;
				m_previousCursorX = mouseX;
				m_previousCursorY = mouseY;

				xoffset *= m_cursorSensitivity * delta;
				yoffset *= m_cursorSensitivity * delta;

				rotateUp(-xoffset);
				rotateRight(yoffset);
			}

			// update camera postion based on keyboard inputs
			if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
				translateFront(m_cameraSpeed * delta);
			if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
				translateFront(-m_cameraSpeed * delta);
			if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
				translateRight(-m_cameraSpeed * delta);
			if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
				translateRight(m_cameraSpeed * delta);
			if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
				translateUp(m_cameraSpeed * delta);
			if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
				translateUp(-m_cameraSpeed * delta);
			if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
				glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		}
	};
}