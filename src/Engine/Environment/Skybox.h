#pragma once

#include "../Textures/Cubemap.h"
#include "../Config/config.h"
#include "../Shaders/ShaderProgram.h"

namespace Engine
{
	class Skybox : public Cubemap
	{
	private:

	public:
		Skybox(ShaderProgram & environmentShader, std::filesystem::path right = DEFAULT_SKYBOX_RIGHT_TEXTURE_PATH, std::filesystem::path left = DEFAULT_SKYBOX_LEFT_TEXTURE_PATH,
			std::filesystem::path top = DEFAULT_SKYBOX_TOP_TEXTURE_PATH, std::filesystem::path bottom = DEFAULT_SKYBOX_BOTTOM_TEXTURE_PATH,
			std::filesystem::path front = DEFAULT_SKYBOX_FRONT_TEXTURE_PATH, std::filesystem::path back = DEFAULT_SKYBOX_BACK_TEXTURE_PATH)
			: Cubemap(environmentShader, right, left, top, bottom, front, back)
		{}

		~Skybox() {}
	};
}