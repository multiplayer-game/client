#pragma once

#include "Skybox.h"
#include "../Shaders/Shader.h"
#include "../Shaders/ShaderProgram.h"
#include "../Shaders/ShaderParser.h"
#include "../Graphics/Camera.h"

namespace Engine
{
	class EnvironmentManager
	{
	private:
		ShaderProgram * m_shaderProgram;
		Shader * m_vertexShader;
		Shader * m_fragmentShader;

		Skybox * m_skybox;

	public:
		EnvironmentManager() {}

		~EnvironmentManager()
		{
			delete m_vertexShader;
			delete m_fragmentShader;
			delete m_shaderProgram;

			delete m_skybox;
		}

		void init()
		{
			ShaderParser * parser = new ShaderParser();
			std::string path = (parser->getShaderPath() / "environmentShaders" / "environment.vert").string();
			std::string file = parser->parseFile(path);
			m_vertexShader = new Shader(path, file, GL_VERTEX_SHADER);
			path = (parser->getShaderPath() / "environmentShaders" / "environment.frag").string();
			file = parser->parseFile(path);
			m_fragmentShader = new Shader(path, file, GL_FRAGMENT_SHADER);
			m_shaderProgram = new ShaderProgram(m_vertexShader, m_fragmentShader);
			delete parser;

			m_skybox = new Skybox(*m_shaderProgram);
		}

		void render(GLFWwindow * window, Camera * camera)
		{
			m_shaderProgram->useProgram();
			updateUniforms(window, camera);

			// Skybox Rendering
			glDepthFunc(GL_LEQUAL);
			m_skybox->render();
			glDepthFunc(GL_LESS);

			m_shaderProgram->unuseProgram();
		}

		void updateUniforms(GLFWwindow * window, Camera * camera)
		{
			m_skybox->updateUniforms(0, "uni_cubemap");

			int width, height;
			glfwGetWindowSize(window, &width, &height);
			glm::mat4 proj_mat = glm::perspective(glm::pi<float>() / 2.0f, (float)width / (float)height, 0.0001f, 10000.0f);

			glm::mat4 view = glm::translate(camera->getInverseTransform(), camera->getPosition());
			GLint uniView = glGetUniformLocation(m_shaderProgram->getId(), "uni_mat_view");
			glUniformMatrix4fv(uniView, 1, GL_FALSE, &view[0][0]);

			GLint uniProjection = glGetUniformLocation(m_shaderProgram->getId(), "uni_mat_proj");
			glUniformMatrix4fv(uniProjection, 1, GL_FALSE, &proj_mat[0][0]);
		}

		const Skybox * getSbybox() { return m_skybox; }
	};
}