#pragma once

#include <filesystem>
#include "../Textures/Texture.h"
#include "../Loaders/TextureLoader.h"
#include "../Shaders/ShaderProgram.h"

namespace Engine
{
	class Material
	{
	private:
		Texture m_diffuse;
		Texture m_specular;
		Texture m_normalMap;
		float m_shininess;

		// TODO : specify default paths for textures
		static std::filesystem::path m_defaultDiffusePath;
		static std::filesystem::path m_defaultSpecularPath;
		static std::filesystem::path m_defaultNormalMapPath;
		float m_defaultShininess;

	public:
		// Creates a material and provides default textures when not specified any
		Material(Texture diffuse = Texture(m_defaultDiffusePath), Texture specular = Texture(m_defaultSpecularPath), Texture normalMap = Texture(m_defaultNormalMapPath), float shininess = 50.0) : m_diffuse(diffuse), m_specular(specular), m_normalMap(normalMap), m_shininess(shininess) {}

		~Material() {}

		const Texture & getDiffuse() { return m_diffuse; }
		const Texture & getSpecular() const { return m_specular; }
		const Texture & getNormalMap() const { return m_normalMap; }
		float getShininesss() const { return m_shininess; }

		void update(GLuint diffuseTextureUnit, GLuint specularTextureUnit, GLuint normalMapTextureUnit, ShaderProgram & program)
		{
			m_diffuse.bindTexture(diffuseTextureUnit);
			GLint diffuseTextureLocation = glGetUniformLocation(program.getId(), "uni_diffuseTexture");
			glUniform1i(diffuseTextureLocation, diffuseTextureUnit);

			m_specular.bindTexture(specularTextureUnit);
			GLint specularTextureLocation = glGetUniformLocation(program.getId(), "uni_specularTexture");
			glUniform1i(specularTextureLocation, specularTextureUnit);

			m_normalMap.bindTexture(normalMapTextureUnit);
			GLint normalMapTextureLocation = glGetUniformLocation(program.getId(), "uni_normalMapTexture");
			glUniform1i(normalMapTextureLocation, normalMapTextureUnit);

			GLint uniShininess = glGetUniformLocation(program.getId(), "uni_shininess");
			glUniform1fv(uniShininess, 1, &m_shininess);
		}

		void unbind()
		{
			m_diffuse.unbindTexture();
			m_specular.unbindTexture();
			m_normalMap.unbindTexture();
		}
	};
}