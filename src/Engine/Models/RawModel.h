#pragma once

#include <GL\GL.h>
#include <GL\glew.h>
#include <vector>
#include <optional>
#include "..\OpenGL\VBO.h"
#include "..\OpenGL\EBO.h"

namespace Engine
{
	class RawModel
	{
	private:
		std::optional<std::vector<glm::vec3>> m_vertices;
		std::optional<std::vector<glm::vec3>> m_normals;
		std::optional<std::vector<glm::vec3>> m_tangents;
		std::optional<std::vector<glm::vec3>> m_bitangents;
		std::optional<std::vector<glm::vec2>> m_textureCoordinates;
		std::optional<std::vector<GLuint>> m_indices;

		VBO * m_verticesVbo;
		VBO * m_normalsVbo;
		VBO * m_tangentsVbo;
		VBO * m_bitangentsVbo;
		VBO * m_textureCoordinatesVbo;
		EBO * m_indicesEbo;

	public:
		// Initializes an empty model
		RawModel()
		{}

		// Sets the vertices
		void setVertices(const std::vector<glm::vec3> & vertices)
		{
			//delete m_verticesVbo;
			m_vertices = vertices;
		}

		// Gets the vertices
		const std::optional<std::vector<glm::vec3>> & getVertices() const { return m_vertices; }

		// Gets the vertices VBO and creates it if needed
		const VBO * getVerticesVbo()
		{
			if (m_verticesVbo == nullptr)
			{
				assert(m_vertices && "No vertices in the mesh data");
				m_verticesVbo = new VBO(*m_vertices);
			}
			return m_verticesVbo;
		}

		// Sets the normals
		void setNormals(const std::vector<glm::vec3> & normals)
		{
			delete m_normalsVbo;
			m_normals = normals;
		}

		// Gets the normals
		const std::optional<std::vector<glm::vec3>> & getNormals() const { return m_normals; }

		// Returns the normals VBO and creates it if needed
		const VBO * getNormalsVbo()
		{
			if (m_normalsVbo == nullptr)
			{
				assert(m_normals && "No normals in the mesh data");
				m_normalsVbo = new VBO(*m_normals);
			}
			return m_normalsVbo;
		}

		// Sets the tangents
		void setTangents(std::vector<glm::vec3> & tangents)
		{
			delete m_tangentsVbo;
			m_tangents = tangents;
		}

		// Gets the tangents
		const std::optional<std::vector<glm::vec3>> & getTangents() const { return m_tangents; }

		// Returns the tangents VBO and creates it if needed
		const VBO * getTangentsVbo()
		{
			if (m_tangentsVbo == nullptr)
			{
				assert(m_tangents && "No tangents in the mesh data");
				m_tangentsVbo = new VBO(*m_tangents);
			}
			return m_tangentsVbo;
		}

		// Sets the bitangents
		void setBitangents(std::vector<glm::vec3> & bitangents)
		{
			delete m_bitangentsVbo;
			m_bitangents = bitangents;
		}

		// Gets the bitangents
		const std::optional<std::vector<glm::vec3>> & getBitangents() const { return m_bitangents; }

		// Returns the bitangents VBO and creates it if needed
		const VBO * getBitangentsVbo()
		{
			if (m_bitangentsVbo == nullptr)
			{
				assert(m_bitangents && "No tangents in the mesh data");
				m_bitangentsVbo = new VBO(*m_bitangents);
			}
			return m_bitangentsVbo;
		}

		// Sets the texture coordinates
		void setTextureCoordinates(const std::vector<glm::vec2> & textureCoordinates)
		{
			delete m_textureCoordinatesVbo;
			m_textureCoordinates = textureCoordinates;
		}

		// Gets the texture coordinates
		const std::optional<std::vector<glm::vec2>> & getTextureCoordinates() const { return m_textureCoordinates; }

		// Returns the texture coordinates VBO and creates it if needed
		const VBO * getTextureCoordinatesVbo()
		{
			if (m_textureCoordinatesVbo == nullptr)
			{
				assert(m_textureCoordinates && "No texture coordinates in the mesh data");
				m_textureCoordinatesVbo = new VBO(*m_textureCoordinates);
			}
			return m_textureCoordinatesVbo;
		}

		// Sets the indices
		void setIndices(const std::vector<GLuint> & indices)
		{
			delete m_indicesEbo;
			m_indices = indices;
		}

		// Gets the indices.
		const std::optional<std::vector<GLuint>> & getIndices() const { return m_indices; }

		// Gets the EBO
		const EBO * getIndicesEbo()
		{
			if (m_indicesEbo == nullptr)
			{
				assert(m_indices && "No indices in the mesh data");
				m_indicesEbo = new EBO(*m_indices);
			}
			return m_indicesEbo;
		}
	};
}
