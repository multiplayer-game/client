#include "../Material.h"
#include "../../Config/config.h"

std::filesystem::path Engine::Material::m_defaultDiffusePath = DEFAULT_DIFFUSE_TEXTURE_PATH;
std::filesystem::path Engine::Material::m_defaultSpecularPath = DEFAULT_SPECULAR_TEXTURE_PATH;
std::filesystem::path Engine::Material::m_defaultNormalMapPath = DEFAULT_NORMALMAP_TEXTURE_PATH;