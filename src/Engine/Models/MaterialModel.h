#pragma once

#include "RawModel.h"
#include "Material.h"
#include "glm/glm.hpp"
#include "../Shaders/ShaderProgram.h"

namespace Engine
{
	class MaterialModel
	{
	private:
		RawModel * m_model;
		Material * m_material;

		glm::mat4 m_modelMatrix;

		VAO * m_vao;

	public:
		MaterialModel(RawModel * model, Material * material) : m_model(model), m_material(material), m_modelMatrix(glm::mat4(1.0f)) {}

		~MaterialModel()
		{
			delete m_model;
			delete m_material;

			delete m_vao;
		}

		void prepareForRender(ShaderProgram & shaderProgram)
		{
			if (m_vao == nullptr)
			{
				std::vector<std::pair<std::string, const VBO *>> vbos;
				vbos.push_back(std::pair("in_position", m_model->getVerticesVbo()));
				vbos.push_back(std::pair("in_texCoords", m_model->getTextureCoordinatesVbo()));
				vbos.push_back(std::pair("in_normal", m_model->getNormalsVbo()));
				vbos.push_back(std::pair("in_tangent", m_model->getTangentsVbo()));
				vbos.push_back(std::pair("in_bitangent", m_model->getBitangentsVbo()));

				m_vao = new VAO(shaderProgram, vbos, m_model->getIndicesEbo());
			}
			m_vao->bind();
		}

		void unbind()
		{
			m_vao->unbind();
			m_material->unbind();
		}

		const Material * getMaterial() { return m_material; }
		const RawModel * getModel() { return m_model; }
		const GLint getEboSize() { return m_vao->eboSize(); }
		const glm::mat4  & getModelMatrix() { return m_modelMatrix; }
		void translate(glm::vec3 translation) { m_modelMatrix = glm::translate(m_modelMatrix, translation); }
		void rotate(glm::vec3 axis, float angle) { m_modelMatrix = glm::rotate(m_modelMatrix, angle, axis); }
		void scale(glm::vec3 scaleVector) { m_modelMatrix = glm::scale(m_modelMatrix, scaleVector); }
		void bindMaterial(GLuint diffuseTextureUnit, GLuint specularTextureUnit, GLuint normalMapTextureUnit, ShaderProgram & program) { m_material->update(diffuseTextureUnit, specularTextureUnit, normalMapTextureUnit, program); }
	};
}