#pragma once
#include <string>

namespace System
{
	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// \class	Path
	///
	/// \brief	Path.
	///
	/// \author	F. Lamarche, Université de Rennes 1
	/// \date	17/12/2015
	////////////////////////////////////////////////////////////////////////////////////////////////////
	class Path
	{
	public:

		////////////////////////////////////////////////////////////////////////////////////////////////////
		/// \fn	inline static ::std::string Path::executable();
		///
		/// \brief	Returns the path of the executable file.
		///
		/// \author	F. Lamarche, Université de Rennes 1
		/// \date	17/12/2015
		////////////////////////////////////////////////////////////////////////////////////////////////////
		static ::std::string executable();
	};
}