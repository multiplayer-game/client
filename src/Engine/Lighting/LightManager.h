#pragma once

#include <vector>
#include "PointLight.h"
#include "DirectionnalLight.h"

namespace Engine
{
	class LightManager
	{
	private:
		std::vector<PointLight> m_pointLights;
		std::vector<DirectionnalLight> m_directionnalLights;

	public:
		LightManager() {}
		~LightManager() {}

		std::vector<PointLight> & getPointLights() { return m_pointLights; }

		const int getPointLightAmount() { return m_pointLights.size(); }

		void addPointLight(PointLight & pointLight) { m_pointLights.push_back(pointLight); }

		std::vector<DirectionnalLight> & getDirectionnalLights() { return m_directionnalLights; }

		const int getDirectionnalLightsAmount() { return m_directionnalLights.size(); }

		void addDirectionnalLight(DirectionnalLight & directionnalLight) { m_directionnalLights.push_back(directionnalLight); }
	};
}