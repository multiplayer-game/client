#pragma once

#include <glm/glm.hpp>
#include "../OpenGL/FBO.h"

namespace Engine
{
	class DirectionnalLight
	{
	private:
		glm::vec3 m_color;
		glm::vec3 m_direction;

		//FBO * m_shadowMap;

	public:
		DirectionnalLight(glm::vec3 color = glm::vec3(253.0 / 255.0, 251.0 / 255.0, 211.0 / 255.0), glm::vec3 direction = glm::normalize(glm::vec3(-1.0, -2.0, 0.0))) : m_color(color), m_direction(direction) {};
		//DirectionnalLight() : m_color(253.0 / 255.0, 251.0 / 255.0, 211.0 / 255.0), m_direction(glm::normalize(glm::vec3(-1.0, -2.0, 0.0))) {};
		~DirectionnalLight() {};

		glm::vec3 getColor() { return m_color; }
		glm::vec3 getDirection() { return m_direction; }

		void renderToShadowMap()
		{
		}

		void updateShadowMapUniforms()
		{
		}

		void setShadowMapResolution(int height, int width)
		{
			//m_shadowMap->resize(height, width);
		}
	};
}