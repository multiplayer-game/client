#pragma once

#include <glm/glm.hpp>

namespace Engine
{
	class PointLight
	{
	private:
		glm::vec3 m_position;
		glm::vec3 m_color;
		float m_intensity;

	public:
		PointLight(glm::vec3 pos, glm::vec3 color, float intensity) : m_position(pos), m_color(color), m_intensity(intensity) {}
		~PointLight() {}

		glm::vec3 getPosition() { return m_position; }
		glm::vec3 getColor() { return m_color; }
		float getIntensity() { return m_intensity; }
	};
}