#pragma once

#include <chrono>
#include "Graphics\WindowManager.h"
#include "Graphics\MasterRenderer.h"
#include "Models\RawModel.h"
#include "Models\Material.h"
#include <GLFW\glfw3.h>

using namespace std;

namespace Engine
{
	class Application
	{
	private:

		WindowManager * m_windowManager;

		MasterRenderer * m_masterRenderer;

		bool m_isRunning;

	public:
		Application() {}

		~Application()
		{
			delete m_windowManager;
			delete m_masterRenderer;
		}

		void init(int height, int width, string name)
		{
			m_windowManager = new WindowManager();
			m_windowManager->createWindow(height, width, name);
			m_masterRenderer = new MasterRenderer();
			m_masterRenderer->init();
			m_isRunning = true;
			gameLoop();
		}

		void exit()
		{
			m_isRunning = false;
		}

		void close()
		{
			m_windowManager->closeWindow();
		}

		void update(long dt)
		{
			if (m_windowManager->windowCloseRequested()) { exit(); }
		}

		void render(long dt, bool disp)
		{
			m_masterRenderer->prepareRender();
			m_masterRenderer->renderAll(m_windowManager->getWindow(), dt, disp);
			m_windowManager->updateWindow();
		}

		void gameLoop()
		{
			auto now = chrono::high_resolution_clock::now();
			auto previous = chrono::high_resolution_clock::now();

			auto elapsed = now - previous;

			auto timeTick = chrono::duration_cast<std::chrono::nanoseconds>(elapsed).count();
			auto timeFrame = chrono::duration_cast<std::chrono::nanoseconds>(elapsed).count();
			auto timeInfo = chrono::duration_cast<std::chrono::nanoseconds>(elapsed).count();

			int fpsInfo = 0;
			int tpsInfo = 0;

			while (m_isRunning)
			{
				previous = now;
				now = chrono::high_resolution_clock::now();

				elapsed = now - previous;

				timeTick += chrono::duration_cast<std::chrono::nanoseconds>(elapsed).count();
				timeFrame += chrono::duration_cast<std::chrono::nanoseconds>(elapsed).count();
				timeInfo += chrono::duration_cast<std::chrono::nanoseconds>(elapsed).count();

				if (timeFrame >= 1000000000 / FPS_CAP)
				{
					render(timeFrame, (fpsInfo == 0 && LOG_RENDER_TIMES));
					timeFrame = 0;
					fpsInfo++;
				}
				if (timeTick >= 1000000000 / TICK_CAP)
				{
					update(timeTick);
					timeTick = 0;
					tpsInfo++;
				}
				if (timeInfo >= 1000000000)
				{
					cout << "[info] : tps : " << tpsInfo << "  fps : " << fpsInfo << std::endl;
					timeInfo = 0;
					tpsInfo = 0;
					fpsInfo = 0;
				}
			}
		}
	};
}