#pragma once

#include <GL/glew.h>
#include <GL/GL.h>
#include <cassert>
#include "Shader.h"
#include "../Config/config.h"

namespace Engine
{
	class ShaderProgram
	{
	private:
		GLuint m_programId;

		void link()
		{
			glLinkProgram(m_programId);
			GLint linkStatus;
			glGetProgramiv(m_programId, GL_LINK_STATUS, &linkStatus);

			if (linkStatus == GL_FALSE)
			{
				GLint infoLogLength;
				glGetProgramiv(m_programId, GL_INFO_LOG_LENGTH, &infoLogLength);
				GLchar * strInfoLog = new GLchar[infoLogLength + 1];
				glGetProgramInfoLog(m_programId, infoLogLength, NULL, strInfoLog);
				if(LOG_DEBUG_MESSAGE) std::cerr << "ShaderProgram: Failed to link shader program. " << std::endl << strInfoLog << std::endl;
				delete[] strInfoLog;
				glDeleteProgram(m_programId);
				m_programId = 0;
			}
		}

	public:
		ShaderProgram(const Shader * vertex, const Shader * fragment)
		{
			assert(vertex->getShaderType() == GL_VERTEX_SHADER && vertex->isValid());
			assert(fragment->getShaderType() == GL_FRAGMENT_SHADER && fragment->isValid());
			m_programId = glCreateProgram();
			glAttachShader(m_programId, vertex->getShaderId());
			glAttachShader(m_programId, fragment->getShaderId());
			link();
		}

		ShaderProgram(const Shader * vertex, const Shader * geometry, const Shader * fragment)
		{
			assert(vertex->getShaderType() == GL_VERTEX_SHADER && vertex->isValid());
			assert(geometry->getShaderType() == GL_GEOMETRY_SHADER && geometry->isValid());
			assert(fragment->getShaderType() == GL_FRAGMENT_SHADER && fragment->isValid());
			m_programId = glCreateProgram();
			glAttachShader(m_programId, vertex->getShaderId());
			glAttachShader(m_programId, geometry->getShaderId());
			glAttachShader(m_programId, fragment->getShaderId());
			link();
		}

		ShaderProgram() : m_programId(0) {}

		~ShaderProgram() { if (m_programId != 0) { glDeleteProgram(m_programId); } }

		bool isValid() const { return m_programId != 0; }

		GLuint getId() const { assert(isValid()); return m_programId; }

		GLint getAttributeLocation(const std::string & name) const { assert(isValid()); return glGetAttribLocation(m_programId, name.c_str()); }

		void useProgram() const { assert(isValid()); glUseProgram(m_programId); }

		void unuseProgram() const { glUseProgram(0); }
	};
}