#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include <filesystem>
#include <sstream>
#include "..\System\Path.h"

namespace Engine
{
	class ShaderParser
	{
	public:
		ShaderParser() {}

		~ShaderParser() {}

		std::string parseFile(const std::filesystem::path & file)
		{
			if (!std::filesystem::exists(file))
			{
				if(LOG_DEBUG_MESSAGE) std::cerr << "File " << file.string() << " does not exists" << std::endl;
			}
			std::stringstream result;
			std::ifstream input(file);
			while (!input.eof())
			{
				std::string tmp;
				std::getline(input, tmp);
				result << tmp << std::endl;
			}
			return result.str();
		}

		std::filesystem::path getShaderPath() { return (std::filesystem::path) System::Path::executable() / ".." / ".." / "res" / "shaders"; }
	};
}