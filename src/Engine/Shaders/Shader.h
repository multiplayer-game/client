#pragma once

#include <GL/glew.h>
#include <GL/GL.h>
#include <iostream>
#include <cassert>
#include "../Config/config.h"

namespace Engine
{
	class Shader
	{
	private:
		GLenum m_shaderType;
		GLuint m_shaderId;
		std::string m_shaderCode;
		std::string m_path;

	public:
		Shader(std::string & path, std::string & shaderCode, GLenum shaderType) : m_path(path), m_shaderCode(shaderCode), m_shaderType(shaderType), m_shaderId(0) { compileShader(); }

		~Shader()
		{
			if (m_shaderId != 0) { glDeleteShader(m_shaderId); }
		}

		void compileShader()
		{
			if(LOG_DEBUG_MESSAGE) std::cout << "Loading shader : " << m_path << " ..." << std::endl;

			m_shaderId = glCreateShader(m_shaderType);

			const char * sourceCode = m_shaderCode.c_str();

			glShaderSource(m_shaderId, 1, &sourceCode, NULL);
			glCompileShader(m_shaderId);

			GLint shaderStatus;
			glGetShaderiv(m_shaderId, GL_COMPILE_STATUS, &shaderStatus);

			if (shaderStatus == GL_FALSE || m_shaderId == 0)
			{
				GLint infoLogLength;
				glGetShaderiv(m_shaderId, GL_INFO_LOG_LENGTH, &infoLogLength);
				GLchar * strInfoLog = new GLchar[infoLogLength + 1];
				glGetShaderInfoLog(m_shaderId, infoLogLength, NULL, strInfoLog);
				if(LOG_DEBUG_MESSAGE) std::cerr << "Shader: shader compilation failed. " << std::endl << strInfoLog << std::endl;
				delete[] strInfoLog;
				glDeleteShader(m_shaderId);
				m_shaderId = 0;
			}
			else
			{
				if(LOG_DEBUG_MESSAGE) std::cout << "Shader compilation successfull : " << ((m_shaderType == GL_VERTEX_SHADER) ? "vertex shader" : "fragment shader") << std::endl;
			}
		}

		GLuint getShaderId() const { assert(isValid()); return m_shaderId; }

		GLenum getShaderType() const { assert(isValid()); return m_shaderType; }

		bool isValid() const { return m_shaderId != 0; }
	};
}