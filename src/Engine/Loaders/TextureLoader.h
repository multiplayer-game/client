#pragma once

#include <map>
#include <filesystem>
#include "GL/glew.h"
#include "GL/GL.h"
#include "../System/Path.h"

namespace Engine
{
	class TextureLoader
	{
	private:
		static std::map<std::filesystem::path, GLuint> m_textures;

	public:
		TextureLoader() {};
		~TextureLoader() {};

		void cleanup()
		{
			for (std::pair<std::filesystem::path, GLuint> texture : m_textures)
			{
				glDeleteTextures(1, &(texture.second));
			}
		}

		GLuint loadTexture(std::filesystem::path & texturePath) const;
		GLuint loadCubemap(std::vector<std::filesystem::path> & cubemapPaths) const;
		static std::filesystem::path getTexturePath() { return (std::filesystem::path) System::Path::executable() / ".." / ".." / "res" / "textures"; }
	};
}