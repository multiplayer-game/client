#include "..\TextureLoader.h"

#include "SOIL/SOIL.h"
#include "GL/glew.h"
#include "GL/GL.h"
#include "../../System/Path.h"
#include <iostream>
#include "../../Config/config.h"

std::map<std::filesystem::path, GLuint> Engine::TextureLoader::m_textures = std::map<std::filesystem::path, GLuint>();

GLuint Engine::TextureLoader::loadTexture(std::filesystem::path & texturePath) const
{
	if (m_textures.count(texturePath) > 0) { return (*m_textures.find(texturePath)).second; }

	std::string stringPath = texturePath.string();
	const char * charPath = stringPath.c_str();

	if(LOG_DEBUG_MESSAGE) std::cout << "Loading texture : " << charPath << std::endl;

	glEnable(GL_TEXTURE_2D);

	GLuint textureID = SOIL_load_OGL_texture(charPath, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_POWER_OF_TWO | SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y);

	if (textureID == 0)
	{
		if(LOG_DEBUG_MESSAGE) printf("SOIL loading error: '%s'\n", SOIL_last_result());
		return -1;
	}
	if (glGetError() != GL_NO_ERROR && LOG_DEBUG_MESSAGE)
	{
		std::cout << "OpenGL error while loading texture ! " << std::endl;
	}

	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);

	m_textures.emplace(texturePath, textureID);

	return textureID;
}

GLuint Engine::TextureLoader::loadCubemap(std::vector<std::filesystem::path> & cubemapPaths) const
{
	if (m_textures.count(cubemapPaths[0]) > 0) { return (*m_textures.find(cubemapPaths[0])).second; }

	if(LOG_DEBUG_MESSAGE) std::cout << "Loading cubemap from texture... Textures :" << std::endl;

	for (int i = 0; i < 6 && LOG_DEBUG_MESSAGE; i++)
	{
		std::cout << "loading " << cubemapPaths[i].string() << std::endl;
	}

	GLuint textureID = SOIL_load_OGL_cubemap(cubemapPaths[0].string().c_str(), cubemapPaths[1].string().c_str(), cubemapPaths[2].string().c_str()
		, cubemapPaths[3].string().c_str(), cubemapPaths[4].string().c_str(), cubemapPaths[5].string().c_str()
		, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_POWER_OF_TWO | SOIL_FLAG_MIPMAPS);

	if (textureID == 0)
	{
		if(LOG_DEBUG_MESSAGE) printf("SOIL loading error: '%s'\n", SOIL_last_result());
		return -1;
	}
	if (glGetError() != GL_NO_ERROR && LOG_DEBUG_MESSAGE)
	{
		std::cout << "OpenGL error while loading texture ! " << std::endl;
	}

	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

	m_textures.emplace(cubemapPaths[0], textureID);

	return textureID;
}