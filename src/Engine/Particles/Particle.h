#pragma once

#include <glm/glm.hpp>
#include "../Config/config.h"

namespace Engine
{
	class Particle
	{
	private:
		glm::vec3 m_position;
		glm::vec3 m_velocity;
		float m_gravityEffect;
		float m_lifeLength;
		float m_rotation;
		float m_scale;
		float m_elapsedTime;

	public:
		Particle(glm::vec3 position, glm::vec3 velocity, float gravityEffect, float lifeLength, float rotation, float scale)
			: m_position(position), m_velocity(velocity), m_gravityEffect(gravityEffect), m_lifeLength(lifeLength), m_rotation(rotation), m_scale(scale), m_elapsedTime(0)
		{
		}

		~Particle() {}

		glm::vec3 getPosition() const { return m_position; }

		float getRotation() const { return m_rotation; }

		float getScale() const { return m_scale; }

		bool update(long dt)
		{
			double interpoledDT = (double)1 / (double)1000000000 * (double)dt;
			m_velocity.y += GRAVITY * m_gravityEffect * interpoledDT;
			glm::vec3 translation = (m_velocity * (float)interpoledDT);
			m_position = m_position + translation;
			m_elapsedTime += interpoledDT;
			return m_elapsedTime < m_lifeLength;
		}
	};
}