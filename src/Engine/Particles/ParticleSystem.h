#pragma once

#include <GL/glew.h>
#include <GL/GL.h>
#include <SOIL/SOIL.h>
#include <glm/glm.hpp>
#include <cassert>
#include <vector>
#include <chrono>
#include "../Shaders/ShaderProgram.h"
#include "../Graphics/Camera.h"
#include "../OpenGL/VAO.h"
#include "../OpenGL/VBO.h"
#include "../OpenGL/EBO.h"
#include "../Models/MaterialModel.h"
#include "Particle.h"

#define RAND_F static_cast <float> (rand()) / static_cast <float> (RAND_MAX)

namespace Engine
{
	class ParticleSystem
	{
	private:
		int m_particlesAmount;

		VAO * m_vao;

		ShaderProgram * m_shaderProgram;

		MaterialModel * m_model;

		std::vector<Particle> m_particles;

		std::vector<glm::vec3> m_vertices = { glm::vec3(-0.5, 0.5, 0), glm::vec3(-0.5, -0.5, 0), glm::vec3(0.5, 0.5, 0), glm::vec3(0.5, -0.5, 0) };
		std::vector<glm::vec2> m_texCoords = { glm::vec2(0.0, 1.0), glm::vec2(0.0, 0.0), glm::vec2(1.0, 1.0), glm::vec2(1.0, 0.0) };
		std::vector<GLuint> m_indices = { 0, 1, 2, 2, 1, 3 };
		GLuint m_textureID;

	public:

		// Creates a particle system
		ParticleSystem(ShaderProgram * shaderProgram) : m_shaderProgram(shaderProgram)
		{
			// TEST STUFF, NEEDS TO GO

			std::vector<std::pair<std::string, const VBO *>> vbos;
			VBO * verticeVBO = new VBO(m_vertices);
			VBO * texCoordsVBO = new VBO(m_texCoords);
			EBO * indicesEBO = new EBO(m_indices);
			vbos.push_back(std::pair("in_position", verticeVBO));
			vbos.push_back(std::pair("in_texCoords", texCoordsVBO));
			m_vao = new VAO(*m_shaderProgram, vbos, indicesEBO);
		}

		~ParticleSystem()
		{
			delete m_shaderProgram;
			delete m_vao;
		}

		void addParticle(Particle & p)
		{
			m_particles.push_back(p);
		}

		void setTexture(GLuint textureID)
		{
			m_textureID = textureID;
		}

		// Renders the particle system
		void render(Camera * camera, GLFWwindow * window, long dt)
		{
			m_shaderProgram->useProgram();

			m_vao->bind();

			//auto beforeSort = std::chrono::high_resolution_clock::now();
			auto sortLambda = [=](Particle & p1, Particle & p2) { return glm::distance(p1.getPosition(), camera->getPosition()) > glm::distance(p2.getPosition(), camera->getPosition()); };
			std::sort(m_particles.begin(), m_particles.end(), sortLambda);
			//auto afterSort = std::chrono::high_resolution_clock::now();
			//std::cout << "Particle sort time : " << (float)std::chrono::duration_cast<std::chrono::nanoseconds>(afterSort - beforeSort).count() / (float)1000000 << "ms" << std::endl;

			glDepthMask(GL_FALSE);

			for (Particle & p : m_particles)
			{
				if (!p.update(dt))
					p = Particle(glm::vec3(0.0, 0.0, -1.0), glm::vec3((RAND_F  * 2.0 - 1.0) * 2.0, RAND_F * 10.0, (RAND_F  * 2.0 - 1.0) * 2.0), 0.05, 3.0, RAND_F - 0.5, 0.2);

				updateUniforms(camera, window, dt, p);
				updateTextures(dt);

				glDrawElements(GL_TRIANGLES, m_vao->eboSize(), GL_UNSIGNED_INT, 0);
			}

			glDepthMask(GL_TRUE);

			glBindTexture(GL_TEXTURE_2D, 0);

			m_vao->unbind();

			m_shaderProgram->unuseProgram();
		}

		void updateUniforms(Camera * camera, GLFWwindow * window, long dt, Particle & p)
		{
			int width, height;
			glfwGetWindowSize(window, &width, &height);
			glm::mat4 projMat = glm::perspective(glm::pi<float>() / 2.0f, (float)width / (float)height, FRONT_CLIPPING_PLANE, FAR_CLIPPING_PLANE);

			glm::mat4 viewMat = camera->getInverseTransform();

			glm::vec3 cameraUp = { viewMat[0][1], viewMat[1][1], viewMat[2][1] };
			glm::vec3 cameraRight = { viewMat[0][0], viewMat[1][0], viewMat[2][0] };

			GLint uniView = glGetUniformLocation(m_shaderProgram->getId(), "uni_mat_view");
			glUniformMatrix4fv(uniView, 1, GL_FALSE, &viewMat[0][0]);

			GLint uniProjection = glGetUniformLocation(m_shaderProgram->getId(), "uni_mat_proj");
			glUniformMatrix4fv(uniProjection, 1, GL_FALSE, &projMat[0][0]);

			GLint uniModel = glGetUniformLocation(m_shaderProgram->getId(), "uni_mat_model");
			glm::mat4 modelMat = glm::translate(glm::mat4(1.0f), p.getPosition());
			modelMat = glm::rotate(modelMat, p.getRotation(), glm::vec3(camera->getOrientation() * glm::vec4(0, 0, 1, 0)));
			modelMat = glm::scale(modelMat, glm::vec3(p.getScale(), p.getScale(), p.getScale()));
			glUniformMatrix4fv(uniModel, 1, GL_FALSE, &modelMat[0][0]);

			GLint uniCameraUp = glGetUniformLocation(m_shaderProgram->getId(), "uni_camera_up");
			glUniform3fv(uniCameraUp, 1, &cameraUp[0]);

			GLint uniCameraRight = glGetUniformLocation(m_shaderProgram->getId(), "uni_camera_right");
			glUniform3fv(uniCameraRight, 1, &cameraRight[0]);

			GLint uniParticlePos = glGetUniformLocation(m_shaderProgram->getId(), "uni_particle_center");
			glUniform3fv(uniParticlePos, 1, &p.getPosition()[0]);

			GLint uniParticleSize = glGetUniformLocation(m_shaderProgram->getId(), "uni_particle_size");
			glUniform1f(uniParticleSize, p.getScale());
		}

		void updateTextures(long dt)
		{
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, m_textureID);
			GLint uniTexture = glGetUniformLocation(m_shaderProgram->getId(), "uni_texture");
			//std::cout << "Texture id : " << m_textureID << std::endl;
			glUniform1i(uniTexture, 0);
		}
	};
}