#pragma once

#include <filesystem>
#include <GL/glew.h>
#include <GL/GL.h>
#include "../Loaders/TextureLoader.h"

namespace Engine
{
	class Texture
	{
	private:
		std::filesystem::path m_path;
		GLuint m_textureID;

	public:
		Texture(std::filesystem::path path, GLuint textureID) : m_path(path), m_textureID(textureID) {}

		Texture(std::filesystem::path path) : m_path(path)
		{
			TextureLoader loader = TextureLoader();
			m_textureID = loader.loadTexture(path);
		}

		Texture(Texture & other) : m_path(other.m_path), m_textureID(other.m_textureID) {}

		~Texture() {}

		void bindTexture(int textureUnit)
		{
			glActiveTexture(GL_TEXTURE0 + textureUnit);
			glBindTexture(GL_TEXTURE_2D, m_textureID);
		}

		void unbindTexture() { glBindTexture(GL_TEXTURE_2D, 0); }
	};
}