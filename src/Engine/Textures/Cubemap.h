#pragma once

#include "GL/glew.h"
#include "GL/GL.h"
#include "glm/glm.hpp"
#include "../OpenGL/VBO.h"
#include "../OpenGL/VAO.h"
#include "../Shaders/ShaderProgram.h"
#include <filesystem>

namespace Engine
{
	class Cubemap
	{
	protected:
		GLuint m_cubemapID;

		std::vector<std::filesystem::path> m_paths;

		VBO * m_vertices;
		EBO * m_indices;
		VAO * m_vao;

		ShaderProgram & m_shaderProgram;

		// ugly as hell but whatever
		std::vector<glm::vec3> getVertices()
		{
			return { glm::vec3(-1.0, -1.0, -1.0),
				glm::vec3(1.0, -1.0, -1.0),
				glm::vec3(1.0, 1.0, -1.0),
				glm::vec3(-1.0, 1.0, -1.0),
				glm::vec3(-1.0, -1.0, 1.0),
				glm::vec3(1.0, -1.0, 1.0),
				glm::vec3(1.0, 1.0, 1.0),
				glm::vec3(-1.0, 1.0, 1.0),
			};
		}

		std::vector<GLuint> getIndices()
		{
			return {
				0, 1, 3, 3, 1, 2,
				1, 5, 2, 2, 5, 6,
				5, 4, 6, 6, 4, 7,
				4, 0, 7, 7, 0, 3,
				3, 2, 7, 7, 2, 6,
				4, 5, 0, 0, 5, 1
			};
		}

	public:
		Cubemap(ShaderProgram & shaderProgram, std::filesystem::path right, std::filesystem::path left, std::filesystem::path top, std::filesystem::path bottom, std::filesystem::path front, std::filesystem::path back)
			: m_shaderProgram(shaderProgram)
		{
			m_paths.push_back(right);
			m_paths.push_back(left);
			m_paths.push_back(top);
			m_paths.push_back(bottom);
			m_paths.push_back(front);
			m_paths.push_back(back);
			TextureLoader loader = TextureLoader();
			m_cubemapID = loader.loadCubemap(m_paths);

			m_vertices = new VBO(getVertices(), GL_STATIC_DRAW);
			m_indices = new EBO(getIndices(), GL_STATIC_DRAW);
			std::vector<std::pair<std::string, const VBO *>> vbos;
			vbos.push_back(std::pair("in_position", m_vertices));
			m_vao = new VAO(shaderProgram, vbos, m_indices);
		}

		~Cubemap()
		{
			delete m_indices;
			delete m_vertices;
			delete m_vao;
		}

		void render()
		{
			m_vao->bind();
			glDrawElements(GL_TRIANGLES, m_vao->eboSize(), GL_UNSIGNED_INT, 0);
		}

		void updateUniforms(GLint cubemapTextureUnit, char * uniformName) const
		{
			glActiveTexture(GL_TEXTURE0 + cubemapTextureUnit);
			glBindTexture(GL_TEXTURE_CUBE_MAP, m_cubemapID);
			GLint cubemapLocation = glGetUniformLocation(m_shaderProgram.getId(), uniformName);
			glUniform1i(cubemapLocation, cubemapTextureUnit);
		}

		void updateUniforms(GLint cubemapTextureUnit, char * uniformName, ShaderProgram & shaderProgram) const
		{
			glActiveTexture(GL_TEXTURE0 + cubemapTextureUnit);
			glBindTexture(GL_TEXTURE_CUBE_MAP, m_cubemapID);
			GLint cubemapLocation = glGetUniformLocation(shaderProgram.getId(), uniformName);
			glUniform1i(cubemapLocation, cubemapTextureUnit);
		}
	};
}