#pragma once

// DEFAULT RESSOURCES
#define DEFAULT_DIFFUSE_TEXTURE_PATH		TextureLoader::getTexturePath() / "default" / "default_diffuse.png"
#define DEFAULT_SPECULAR_TEXTURE_PATH		TextureLoader::getTexturePath() / "default" / "default_specular.bmp"
#define DEFAULT_NORMALMAP_TEXTURE_PATH		TextureLoader::getTexturePath() / "default" / "default_normalMap.jpg"
#define DEFAULT_BLACK_TEXTURE_PATH			TextureLoader::getTexturePath() / "default" / "black.bmp"
#define DEFAULT_WHITE_TEXTURE_PATH			TextureLoader::getTexturePath() / "default" / "white.bmp"
#define DEFAULT_MIRROR_TEXTURE_PATH			TextureLoader::getTexturePath() / "mirror.png"
//#define DEFAULT_NORMALMAP_TEXTURE_PATH		TextureLoader::getTexturePath() / "funky_normalMap.jpg"

#define DEFAULT_SKYBOX_RIGHT_TEXTURE_PATH	TextureLoader::getTexturePath() / "skybox" / "right.jpg"
#define DEFAULT_SKYBOX_LEFT_TEXTURE_PATH	TextureLoader::getTexturePath() / "skybox" / "left.jpg"
#define DEFAULT_SKYBOX_TOP_TEXTURE_PATH		TextureLoader::getTexturePath() / "skybox" / "top.jpg"
#define DEFAULT_SKYBOX_BOTTOM_TEXTURE_PATH	TextureLoader::getTexturePath() / "skybox" / "bottom.jpg"
#define DEFAULT_SKYBOX_BACK_TEXTURE_PATH	TextureLoader::getTexturePath() / "skybox" / "back.jpg"
#define DEFAULT_SKYBOX_FRONT_TEXTURE_PATH	TextureLoader::getTexturePath() / "skybox" / "front.jpg"

// MATH STUFF
#define RAND_F					static_cast <float> (rand()) / static_cast <float> (RAND_MAX)
#define M_PI					3.14159265358979323846

// RENDERING STUFF
#define FAR_CLIPPING_PLANE		5.0f
#define FRONT_CLIPPING_PLANE	0.0001f

// PHYSICS STUFF
#define GRAVITY					-50.0f

// SCENE STUFF
#define LIGHT_COUNT				0			// MUST BE <= 32 OTHERWISE SHADERS WILL BREAK

// ENGINE STFF
#define FPS_CAP					240			// MAXIMUM FRAMES PER SECOND
#define TICK_CAP				60			// MAXIMUM TICKS PER SECOND

// DEBUG & PROFILING STUFF
#define LOG_DEBUG_MESSAGE		true		// ALLOWS FOR DEBUG MESSAGE LOGGING
#define LOG_RENDER_TIMES		true		// ALLOWS FOR RENDER TIME MESSAGE LOGGING